package gettingstarted.servingmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServingMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServingMvcApplication.class, args);
	}

}
